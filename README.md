# SpaceBusOS

A Linux distro using NixOS technologies targeted at being an amazing development experiance for phones, tablets, laptops, and desktops alike. 

## Status: Bassicaly just an idea

## IMPORTANT: This project will be used to compete in a science fair. 

## Features:

- [ ] Simple install of a preconfigured NixOS system.
- [ ] Full customisability of the preconfigured systems for anyone willing to dig into the configuration.nix file. 
- [ ] Home Manager and flakes installed by default. 
- [ ] Amazing development tools installed by default like QuickEmu, and Devshells. 
- [ ] A pre configured Plasma Desktop (laptop and desktop editions) with a tiling window manager extension. 
- [ ] A pre configured Plasma Mobile Desktop (tablet and phone editions) with optimisations for coding awesome things, on the go. 
- [ ] Custom tools which integrate with services such as Home manager, and nixpkgs
- [ ] Pre configured applications such as Brave, Starship, and Git. 

For any additional feature requests oppen an isue. 



See the [Wiki](https://fedx-sudo.github.io/SpaceBusOS/) for more details. 
